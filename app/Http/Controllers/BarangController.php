<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\barang;

class BarangController extends Controller
{
    //index (view)
    public function index()
    {
        return barang::all();

    }

    public function create(Request $request)
    {
        $barang = new barang();
        $barang->nama_barang = $request->nama_barang;
        $barang->deskripsi_barang = $request->deskripsi_barang;
        $barang->save();

        return "Data berhasil Masuk";
    }

    public function update(Request $request, $id)
    {
        $nama_barang = $request->nama_barang;
        $deskipsi_barang = $request->deskripsi_barang;

        $barang = barang::find($id);
        $barang->nama_barang = $request->nama_barang;
        $barang->deskripsi_barang = $request->deskripsi_barang;
        $barang->save();

        return "Data berhasil di Update";

    }

    public function delete($id)
    {
        $barang = barang::find($id);
        $barang->delete();

        return "Data berhasil dihapus";
    }
}
